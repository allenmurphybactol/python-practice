#Variable Assignments
num1 = 69
num2 = 420
result = num1 + num2
print (result)

name = 'Pythighs'
print ('Hello ' + name)

#Global and Local Variables
global_var = 10

def local_function():
    local_var = 5
    print (local_var)

print (global_var)
#print (local_var) will not be recognized.

#Must do
local_function()

def modify_global():
    global global_var
    global_var = 20


print(global_var) # --- Without calling the function it will not work.

modify_global()
print(global_var)

#Arithmetic Operators
num1 = 739
num2 = 911

def operator(num1, num2):
    sum = num1 + num2
    difference = num1 - num2
    product = num1 * num2

    if num2 != 0:
        quotient = num1 / num2
    else:
        quotient = "Cannot divide by zero."

    return sum, difference, product, quotient
operation = operator(num1, num2)

print("Here are the answers: ")
print(operation)

base = 8
height = 6
area = base * height / 2
print (area)

#Augmented Assignment Operators
count = 5
count += 3
count *= 2
print (count)

price = 100
discount_percentage = 20
discount_amount = (price * discount_percentage) / 100
price -= discount_amount
print(price)

#Comparison Operators
x = 69
y = 420

if x > y:
    print("x big")
else:
    print("x puny")

str1 = 'Feed my children.'
str2 = 'Feed, my children.'

if str1 == str2:
    print("They're the same.")
else:
    print("Totally different.")

#List and List Manipulation

fruits = ['apple', 'banana', 'grape']
apple = 'apple'
result = apple in fruits
print(result)

sentence = "Whatever in the world you were saying."
world = 'world'
check = world not in sentence
print(check)

#List and List Manipulation
my_list = []
my_list.append(5)
my_list.append(10)
my_list.append(15)
print (my_list)

numbers = [1, 2, 3, 4, 5]
numbers.pop(4)
print(numbers)

#Sublist Using Slices
number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
slicer = slice(2, 7)
print(number_list[slicer])

sentence = 'A quick brown fox jumps over the lazy dog.'
print(sentence[2:7])

#Getting the index
colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
find_green = colors.index('green')
print(find_green)

fruits = ['apple', 'banana', 'grape', 'cherry', 'mango']
find_mango = fruits.index('mango')
print(find_mango)

#Tuple
coordinates = (36, 104)
change = list(coordinates)
change[0], change[1] = 52, 19
coordinates = tuple(change)
print(coordinates)

def quotient_modulo(dividend, divisor):
    quotient = dividend // divisor
    remainder = dividend % divisor
    return (quotient, remainder)

print(quotient_modulo(10, 4))

#Dictionaries
dict = {
    "name": "Erika",
    "age": 20,
    "city": "California"
}

print(dict["name"])

dict ["city"] = "Okinawa"
print(dict)

#range() Function
one_to_ten = range(1, 11)
for i in one_to_ten:
  print (i)

even_numeros = range(2, 21, 2)
for i in even_numeros:
    print (i)

#print() Function
print('Hello, World!')
fruits = ['apple', 'banana', 'grape', 'cherry', 'mango', 'orange']
print(', '.join(fruits))

#input() Function

"""
print("Enter your name: ")
input = input()
print('Hello, ' + input)

dos_numeros1 = float(input("Enter the first number: "))
dos_numeros2 = float(input("Enter the second number: "))
product = dos_numeros1 * dos_numeros2
print(product)

"""

#len() Function
books = ("No Longer Human", "The Metamorphosis", "The Little Prince")
list_counter = len(books)
print(list_counter)

"""

user_input = input('Write a sentence here: ')
characounter = len(user_input)
print(f"The amount of characters you typed: {characounter}")

"""

#continue / break Statements

even_numeros = range(1, 21)

for i in even_numeros:
    if i % 2 != 0:
        continue
    print(i)

for i in even_numeros:
    if i == 15:
        break
    if i % 2 != 0:
        continue
    print(i)

#While and For Loops

loop = 1
while loop < 11:
    print(loop)
    loop += 1

guests = ('Marie', 'Jenny', 'Abraham', 'Blake')
for i in guests:
    print(f'Welcome to the party, {i}!')

#Writing Functions
def add_numbers(addend1, addend2):
    sum = addend1 + addend2
    return sum

sum = add_numbers(69, 420)
print(sum)

def find_max(numeros):
    found_max = max(numeros)
    return found_max

lista_de_numeros = [2, 4 ,6, 8]
here = find_max(lista_de_numeros)
print(here)
